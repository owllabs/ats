 <?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>


            
            <div class="container">
			<div class="row"><div class="col-md-12">
			<div class="section"><div class="container">
			<div class="row"><div class="col-md-6"><div class="section"> 
			<div class="container"> <div class="row"> <div class="col-md-12">
			<h1 class="text-center">Login</h1> </div></div>
			<div class="row"> <div class="col-md-offset-3 col-md-6">

<form method="post" action="<?php echo base_url()."pages/login_validation" ?>" class="form">
         

        <?php
	    echo "<span class='red errors'>";
        echo validation_errors();
	    echo "</span>";
        ?>

        <div class="field">
            <label>Email</label>
            <input type="text" name="email" value="<?php echo set_value('email'); ?>" size="50" />
        </div>

        <div class="field">
            <label>Password</label>
            <input type="password" name="password" value="<?php echo set_value('password'); ?>" size="50" />
        </div>
        <div class="field">
            <button type="submit" class="button blue">Login</button>
        </div>
		
       
        <a href='<?php echo base_url()."pages/register";?>'>Create account</a>

    </form>
 
   
	
	
			</div></div></div></div></div><div class="col-md-6"></div></div></div></div></div></div></div>
        </div>
        
      
 

</body></html>
	 
        